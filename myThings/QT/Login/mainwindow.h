#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    /**
     * @brief on_pushButton_clicked This is the function that controls what happens after pushing the 1 button
     * This is the button that the user presses if he want's to save his Sign Up Information for future logins
     */
    void on_pushButton_clicked(); // Second Sign Up Button

    /**
     * @brief on_pushButton_2_clicked This is the function that controls what happens after pushing the 2 button
     * This is the button that the user presses if he want's to do a login and already has the information stored
     */
    void on_pushButton_2_clicked(); // Second Login Button

    /**
     * @brief on_pushButton_3_clicked This is the function that controls what happens after pushing the 3 button
     * This is the button that the user presses if he want to go to the Sign Up page
     */
    void on_pushButton_3_clicked(); // First Sign Up Button

    /**
     * @brief on_pushButton_4_clicked This is the function that controls what happens after pushing the 4 button
     * This is the button that the user presses if he want's to go to the Login Page
     */
    void on_pushButton_4_clicked(); // First Login Button

private:
    Ui::MainWindow *ui;
    QString fileName = "UserData";
};
#endif // MAINWINDOW_H
