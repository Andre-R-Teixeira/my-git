#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QWidget>
#include <QString>

namespace Ui {
class Calculator;
}

class Calculator : public QWidget
{
    Q_OBJECT

public:
    explicit Calculator(QWidget *parent = nullptr);
    ~Calculator();

private slots:
    void on_pushButton_clicked();   // number 0
    void on_pushButton_1_clicked(); // number 1
    void on_pushButton_2_clicked(); // number 2
    void on_pushButton_3_clicked(); // number 3
    void on_pushButton_4_clicked(); // number 4
    void on_pushButton_5_clicked(); // number 5
    void on_pushButton_6_clicked(); // number 6
    void on_pushButton_7_clicked(); // number 7
    void on_pushButton_8_clicked(); // number 8
    void on_pushButton_9_clicked(); // number 9

    void on_pushButton_11_clicked(); // -
    void on_pushButton_12_clicked(); // +
    void on_pushButton_13_clicked(); // /
    void on_pushButton_14_clicked(); // *
    void on_pushButton_15_clicked(); // Clear
    void on_pushButton_16_clicked(); // Result

private:
    Ui::Calculator *ui;
    double First;
    double Second;
    double Result;
    std::string Number;
};

#endif // CALCULATOR_H
