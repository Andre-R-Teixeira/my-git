#include <QCoreApplication>
#include "calculator.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Calculator window;

    window.show();

    return a.exec();
}
