#ifndef HEADERS
#define HEADERS

#define MAX 50

#include <iostream>
#include <list>
#include <string>
#include <stdlib.h>
using namespace std;

class Vehicle
{
    private:
        /* data */
    protected:
        int wheels;
        int serial;
        int price;
        string brand;
        virtual int get_wheels_count()
        {
            return wheels;
        }
    public:
        virtual void set_serial(int s)
        {
            serial = s;
        }
        virtual void set_price(int p)
        {
            price = p;
        }
        virtual void set_brand(string b)
        {
            brand = b;
        }
		virtual void set_Wheels(int w) 
		{
			wheels  = w;
		}
        virtual int get_serial()
        {
            return serial;
        }
        virtual int get_price()
        {
            return price;
        }
        virtual string get_brand()
        {
            return brand;
        }
};
class Car : public Vehicle
{
    private:
		//no private data
    public:
        int get_wheels_count() override
        {
            return wheels;
        }
        void set_serial(int s) override
        {
            serial = s;
        }
        void set_price(int p) override
        {
            price = p;
        }
		void set_Wheels(int w) override
		{
			wheels  = w;
		}
        void set_brand(string b) override
        {
            brand = b;
        }
        int get_serial() override
        {
            return serial;
        }
        int get_price() override
        {
            return price;
        }
        string get_brand() override
        {
            return brand;
        }

};

class Motorcycle : public Vehicle
{
    private:
        int wheels = 2;
    public:
        int get_wheels_count() override
        {
            return wheels;
        }
        void set_serial(int s) override
        {
            serial = s;
        }
        void set_price(int p) override
        {
            price = p;
        }
        void set_brand(string b) override
        {
            brand = b;
        }
        int get_serial() override
        {
            return serial;
        }
        int get_price() override
        {
            return price;
        }
        string get_brand() override
        {
            return brand;
        }

};

class CarDealer
{
    private:
        list<Motorcycle*> MOTO_List;
        list<Car*> CAR_List;
    public:
        //Motorcycle* MotoHead;
        //Car* CarHead;
        void add_vehicle();
        void remove_vehicle();
        void edit_vehicle();
        void print_vehicles();
        void delete_all_vehicles();
        int check_Moto_serial(int Serial);
        int check_Car_serial(int Serial);

};

#endif