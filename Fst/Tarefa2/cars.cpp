#include "cars.hpp"

void CarDealer::add_vehicle()
{
    int wheels = 0;
    int price = 0;
    int serial = 0;
    int confirmaSerial = 0;
    string brand;

    cout << "Escreva o numero de rodas" << endl;
    cin >> wheels;
    // Confirma o numero de rodas
    while (wheels < 2)
    {
        cout << "Escreva o numero de rodas" << endl;
        cin >> wheels;
        cout << "Por favor meta um numero de rodas que saiba ser lido, maior ou igual a 2" << endl;
    }
    cout << "Escreva o preço " << endl;
    cin >> price;

    cout << "Escreva a marca " << endl;
    cin >> brand;

    cout << "Escreva o serial " << endl;
    cin >> serial;

    if (wheels == 2)
    {
        Motorcycle *newMoto = new (Motorcycle);

        confirmaSerial = check_Moto_serial(serial);

        while (confirmaSerial == 1)
        {
            cout << "Tem de escolher um novo Serial number, este ja está ocupado" << endl;
            cin >> serial;
            confirmaSerial = check_Moto_serial(serial);
        }
        confirmaSerial = 0;

        newMoto->set_serial(serial);
        newMoto->set_price(price);
        newMoto->set_brand(brand);

        MOTO_List.push_back(newMoto);

        cout << "A moto com os valores seguintes foi criada:" << endl
             << newMoto->get_price() << " " << newMoto->get_serial() << " " << newMoto->get_brand() << endl;
    }
    else
    {
        Car *newCar = new (Car);

        confirmaSerial = check_Car_serial(serial);

        while (confirmaSerial == 1)
        {
            cout << "Tem de escolher um novo Serial number, este ja está ocupado" << endl;
            cin >> serial;
            confirmaSerial = check_Car_serial(serial);
        }

        confirmaSerial = 0;

        newCar->set_serial(serial);
        newCar->set_price(price);
        newCar->set_brand(brand);
        newCar->set_Wheels(wheels);

        CAR_List.push_back(newCar);

        cout << "A carro com os valores seguintes foi criada:" << endl
             << brand << " " << serial << " " << price << endl;
    }
    return;
}

void CarDealer::remove_vehicle()
{
    int Serial = 0;
    int wheels = 0;

    cout << "escreva o numero de rodas do veiculo que quer remover" << endl;
    cin >> wheels;

    while (wheels < 2)
    {
        cout << "O numero de rodas tem de ser maior que 1" << endl;
        cin >> wheels;
    }

    cout << "Escreva o serial number do carro/Mota que quer remover" << endl;
    cin >> Serial;

    if (wheels == 2)
    {
        for (auto MotoNow = MOTO_List.cbegin(); MotoNow != MOTO_List.cend(); MotoNow++)
        {
            if ((*MotoNow)->get_serial() == Serial)
            {
                MOTO_List.remove(*(MotoNow));
                cout << "Moto com o serial " << Serial << " Foi removida" << endl;
                delete (*MotoNow);
                return;
            }
        }
        cout << "O serial number dado não corresponde a nenhum valor que já esteja na lista, por favor confirme" << endl;
    }
    else
    {
        for (auto CarNow = MOTO_List.cbegin(); CarNow != MOTO_List.cend(); CarNow++)
        {
            if ((*CarNow)->get_serial() == Serial)
            {
                MOTO_List.remove(*(CarNow));
                cout << "Moto com o serial " << Serial << " Foi removida" << endl;
                delete (*CarNow);
                return;
            }
        }
        cout << "O serial number dado não corresponde a nenhum valor que já esteja na lista, por favor confirme" << endl;
    }
}

void CarDealer::edit_vehicle()
{
    int Serial = 0;
    int wheels = 0;
    int Price = 0;
    string brand;
    int confirmaSerial = 0;

    cout << "escreva o numero de rodas do veiculo que quer remover" << endl;
    cin >> wheels;

    while (wheels < 2)
    {
        cout << "O numero de rodas tem de ser maior que 1" << endl;
        cin >> wheels;
    }

    cout << "Escreva o serial number do carro/Mota que quer remover" << endl;
    cin >> Serial;

    if (wheels == 2)
    {
        for (auto MotoNow = MOTO_List.cbegin(); MotoNow != MOTO_List.cend(); MotoNow++)
        {
            if ((*MotoNow)->get_serial() == Serial)
            {
                cout << "Preço" << endl;
                cin >> Price;
                cout << "Brand" << endl;
                cin >> brand;
                cout << "new Serial" << endl;
                cin >> Serial;

                confirmaSerial = check_Moto_serial(Serial);

                while (confirmaSerial == 1)
                {
                    cout << "Tem de escolher um novo Serial number, este ja está ocupado" << endl;
                    cin >> Serial;
                    confirmaSerial = check_Moto_serial(Serial);
                }

                confirmaSerial = 0;

                (*MotoNow)->set_brand(brand);
                (*MotoNow)->set_serial(Serial);
                (*MotoNow)->set_price(Price);
            }
        }
        cout << "O serial number dado não corresponde a nenhum valor que já esteja na lista, por favor confirme" << endl;
    }
    else
    {
        for (auto CarNow = MOTO_List.cbegin(); CarNow != MOTO_List.cend(); CarNow++)
        {
            if ((*CarNow)->get_serial() == Serial)
            {
                cout << "Preço" << endl;
                cin >> Price;
                cout << "Brand" << endl;
                cin >> brand;
                cout << "new Serial" << endl;
                cin >> Serial;

                confirmaSerial = check_Car_serial(Serial);

                while (confirmaSerial == 1)
                {
                    cout << "Tem de escolher um novo Serial number, este ja está ocupado" << endl;
                    cin >> Serial;
                    confirmaSerial = check_Car_serial(Serial);
                }

                confirmaSerial = 0;

                (*CarNow)->set_brand(brand);
                (*CarNow)->set_serial(Serial);
                (*CarNow)->set_price(Price);
            }
        }
        cout << "O serial number dado não corresponde a nenhum valor que já esteja na lista, por favor confirme" << endl;
    }
}

void CarDealer::print_vehicles()
{
    cout << "Type       | Wheels count |   Serial   |   Brand   |   Price" << endl;

    if (MOTO_List.empty())
    {
    }
    else
    {
        for (auto MOTONow = MOTO_List.cbegin(); MOTONow != MOTO_List.cend(); MOTONow++)
        {
            cout << "Mota"
                 << "\t" << (*MOTONow)->get_wheels_count() << "\t" << (*MOTONow)->get_serial() << "\t" << (*MOTONow)->get_brand() << "\t" << (*MOTONow)->get_price() << "\t" << endl;
        }
    }

    if (CAR_List.empty())
    {
    }
    else
    {
        for (auto CARNow = CAR_List.cbegin(); CARNow != CAR_List.cend(); CARNow++)
        {
            cout << "Carro"
                 << "\t" << (*CARNow)->get_wheels_count() << "\t" << (*CARNow)->get_serial() << "\t" << (*CARNow)->get_brand() << "\t" << (*CARNow)->get_price() << "\t" << endl;
        }
    }

    cout << endl;
}

void CarDealer::delete_all_vehicles()
{
    Car *holdMeCar = NULL;
    Motorcycle *holdMeMoto = NULL;

    if (!(CAR_List.empty()))
    {
        auto freeMeCar = CAR_List.cbegin();
        while (freeMeCar != CAR_List.cend())
        {
            holdMeCar = (*freeMeCar);
            freeMeCar++;
            CAR_List.remove(holdMeCar);
            delete holdMeCar;
        }
    }

    if (!(MOTO_List.empty()))
    {
        auto freeMeMoto = MOTO_List.cbegin();
        while (freeMeMoto != MOTO_List.cend())
        {
            holdMeMoto = (*freeMeMoto);
            freeMeMoto++;
            MOTO_List.remove(holdMeMoto);
            delete holdMeMoto;
        }
    }
}

int CarDealer::check_Car_serial(int Serial)
{
    for (auto CarNow = CAR_List.cbegin(); CarNow != CAR_List.cend(); CarNow++)
    {
        if ((*CarNow)->get_serial() == Serial)
        {
            return 1;
        }
    }
    return 0;
}

int CarDealer::check_Moto_serial(int Serial)
{
    for (auto MotoNow = MOTO_List.cbegin(); MotoNow != MOTO_List.cend(); MotoNow++)
    {
        if ((*MotoNow)->get_serial() == Serial)
        {
            return 1;
        }
    }
    return 0;
}
