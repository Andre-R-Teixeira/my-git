#ifndef TAREFA1
#define TAREFA1

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct car {
    int serial;
    float price;
    char brand[20];
    struct car* next;
} CAR;

CAR *deletCar(CAR *start, int Serial_number);
void editCar(CAR *start, int Serial_number);
CAR *carSelector (CAR *start, int mode);
void PrintList(CAR *start);
CAR *AddCar(CAR *previous);
void Cleanup (CAR *start);
int confirmInput(CAR *start, int serialNumber);

#endif 