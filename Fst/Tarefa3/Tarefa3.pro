# Created by and for Qt Creator This file was created for editing the project sources only.
# You may attempt to use it for building too, by modifying this file here.

#TARGET = Tarefa3

QT = core gui widgets

HEADERS = \
   $$PWD/final/addwindow.h \
   $$PWD/final/addwindow.hpp \
   $$PWD/final/cars.hpp \
   $$PWD/final/mainwindow.h \
   $$PWD/final/moc_predefs.h \
   $$PWD/final/teste.h \
   $$PWD/final/ui_addwindow.h \
   $$PWD/final/ui_mainwindow.h

SOURCES = \
   $$PWD/final/addwindow.cpp \
   $$PWD/final/addwindow.ui \
   $$PWD/final/cars.cpp \
   $$PWD/final/main.cpp \
   $$PWD/final/mainwindow.cpp \
   $$PWD/final/mainwindow.ui \
   $$PWD/final/moc_addwindow.cpp \
   $$PWD/final/moc_mainwindow.cpp \
   $$PWD/final/teste.cpp

INCLUDEPATH = \
    $$PWD/final

#DEFINES = 

